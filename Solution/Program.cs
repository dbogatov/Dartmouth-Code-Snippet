﻿/*
 * I have chosen this C# code snippet because despite its small size it shows the accuracy, care and proficiency of its 
 * author. This snippet is a modification of what I have sent to a company as my coding interview homework. Despite the 
 * actual solution to the problem, this piece of code includes: good conventional coding style and naming conventions, 
 * extensive yet laconic comments and full-coverage test cases. This is a scaled version of how a piece of code 
 * written by professional should look like. By the way, I have got an offer from that company.
 * 
 * Problem statement: write a function that takes a string and produces the first non-repeated character in that string. 
 * Case should not influence the result. Resulting character should be lower-case. If no non-repeated character found, 
 * throw an exception.
 */

// This file - Program.cs was created by Dmytro Bogatov (dmytro@dbogatov.org)
// For Dartmouth graduate school application
// on 11/28/2016, 6:21 PM

using System;
using System.Collections.Generic;

namespace Solution
{
	public class CharacterSolution
	{
		/// <summary>
		/// Returns the first non repeated character.
		/// </summary>
		/// <returns>The first non repeated character.</returns>
		/// <param name="input">Input string in which the first non-repeated character will be found</param>
		public static char GetFirstNonRepeatedCharacter(string input)
		{
			// Use HashMap as an underlying data structure
			// Key is a character, vlaue is a number of times it occurs int he input string
			var charcterCounts = new Dictionary<char, int>();

			// Meet "case-insensitive" requirement
			input = input.ToLower();

			// Populate the hashmap with keys and values
			foreach (var c in input)
			{
				int occurances = 0;
				if (charcterCounts.TryGetValue(c, out occurances))
				{
					// If key is already in the hashmap, increase the value by one (record one more occurrence)
					charcterCounts[c]++;
				}
				else
				{
					// Otherwise, put the key into the hashmap and record its first occurrence
					charcterCounts.Add(c, 1);
				}

				// Shorter, less descriptive version
				// charcterCounts[c] = (charcterCounts.TryGetValue(c, out occurances) ? charcterCounts[c] + 1 : 1);
			}

			// HaskMap is an unordered collection
			// We need to find the first non-repeated character
			foreach (var c in input)
			{
				if (charcterCounts[c] == 1)
				{
					return c;
				}
			}

			// As per requirements, throw an exception if no non-repeated characters found
			throw new Exception("No non-repeated characters found");
		}
	}
}
