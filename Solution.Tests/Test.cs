﻿using NUnit.Framework;
using System;

namespace Solution.Tests
{
	[TestFixture]
	public class Test
	{
		[Test]
		public void TestSimpleScenarios()
		{
			// The most basic case
			Assert.AreEqual(CharacterSolution.GetFirstNonRepeatedCharacter("dartmouth"), 'd');
			// Case, when non-repeted character is far in the end
			Assert.AreEqual(CharacterSolution.GetFirstNonRepeatedCharacter("graduate and undergraduate schools"), 'c');
			// Make sure spaces count as characters
			Assert.AreEqual(CharacterSolution.GetFirstNonRepeatedCharacter("bob wobbs"), ' ');
		}

		[Test]
		public void TestUndefinedBehaivior()
		{
			// No non-repeated characters in the empty string
			Assert.Throws<Exception>(
				() => CharacterSolution.GetFirstNonRepeatedCharacter("")
			);

			// No non-repeated characters in this non-empty string
			Assert.Throws<Exception>(
				() => CharacterSolution.GetFirstNonRepeatedCharacter("www")
			);
		}

		[Test]
		public void TestCaseInsensitive()
		{
			// Make sure the case does not change the result
			Assert.AreEqual(CharacterSolution.GetFirstNonRepeatedCharacter("Gog"), 'o');
			Assert.AreEqual(CharacterSolution.GetFirstNonRepeatedCharacter("GOg"), 'o');
			Assert.AreEqual(CharacterSolution.GetFirstNonRepeatedCharacter("GOG"), 'o');
		}
	}
}
